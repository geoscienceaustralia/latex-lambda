import os
import io
import shutil
import subprocess
import base64
import zipfile
import requests


def lambda_handler(event, context):

    # Extract input ZIP file to /tmp/latex...
    shutil.rmtree("/tmp/latex", ignore_errors=True)
    os.mkdir("/tmp/latex")

    if 'input_url' in event:
        headers = {}
        bytes = requests.get(event['input_url']).content
    else:
        bytes = base64.b64decode(event["input"])

    zipfile_content = zipfile.ZipFile(io.BytesIO(bytes))
    zipfile_content.extractall(path="/tmp/latex")

    os.environ['PATH'] += ":/var/task/texlive/2017/bin/x86_64-linux/"
    os.environ['HOME'] = "/tmp/latex/"
    os.environ['PERL5LIB'] = "/var/task/texlive/2017/tlpkg/TeXLive/"

    os.chdir("/tmp/latex/")

    # Run pdflatex...
    result_shell = subprocess.run(["latexmk",
                                   "-verbose",
                                   "-interaction=batchmode",
                                   "-f",
                                   "-pdf",
                                   "-output-directory=/tmp/latex",
                                   "document.tex"],
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT)

    if "output_url" in event:
        url = event['output_url']
        headers = {'Content-type': 'application/pdf'}

        files = {'document': open('document.pdf', 'rb')}
        response = requests.put(url, files=files, headers=headers)
        return {
            "response": response.status_code,
            "reason": response.reason
        }
    else:
        # Read "document.pdf"...
        with open("document.pdf", "rb") as filecontent:
            pdf = filecontent.read()

        # Return base64 encoded pdf and stdout log from pdflaxex...
        return {
            "output": base64.b64encode(pdf).decode('ascii'),
            "stdout": r.stdout.decode('utf_8')
        }
