#!/usr/bin/env bash
set -e
cd test/system_test
export DEPLOY_ENVIRONMENT=$1
python3 -m pytest -s system_test.py


