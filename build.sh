#!/usr/bin/env bash
set -e
# compile the latex and python packages
docker build -t geoscienceaustralia/latexlambda .
# Create the zip file for deploy
docker run --rm -v "$(pwd)":/var/host geoscienceaustralia/latexlambda zip --symlinks -r -9 /var/host/latex-lambda.zip .
# unit Test
cd test/unit_test
python3 -m pytest pdf_test.py -s
