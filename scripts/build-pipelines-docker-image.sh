#!/usr/bin/env bash

# turn on strict bash
set -euo pipefail

organization=geoscienceaustralia
repository=latex-lambda-pipelines
username=geodesyarchive
password="$(credstash get geodesy_archive_docker_hub_key)"

docker login -u "$username" -p "$password"
docker build -t "$organization/$repository" -f bitbucket-pipelines.dockerfile .
docker push "$organization/$repository"
