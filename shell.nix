let
  pkgs = import ./nixpkgs-pinned {};

in

let
  devEnv = with pkgs; buildEnv {
    name = "devEnv";
    paths = [
      awscli
      cacert
      docker
      docker-compose
      python3
      python3Packages.credstash
      python3Packages.pip
      python3Packages.pylint
      python3Packages.setuptools 
      python3Packages.virtualenv
      shellcheck
      terraform
      imagemagick7
      pdftk
      ghostscript
    ];
  };
in
  pkgs.runCommand "setupEnv" {
    buildInputs = [
      devEnv
    ];
    shellHook = ''
      export SSL_CERT_FILE="${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
      if [[ -e ./aws/aws-env.sh ]]; then
        . ./aws/aws-env.sh
      fi
      virtualenv python-env
      . ./python-env/bin/activate
      pip install -r ./requirements.txt
     '';
  } ""

