requests==2.31.0
pytest==7.4.0
autopep8==2.0.2
boto3==1.28.3
