FROM nixos/nix:2.2.1
RUN nix-env -iA nixpkgs.bash
ADD ./shell.nix /shell.nix
ADD ./nixpkgs-pinned.json /nixpkgs-pinned.json
ADD ./nixpkgs-pinned /nixpkgs-pinned
RUN nix-shell /shell.nix --run "true"
