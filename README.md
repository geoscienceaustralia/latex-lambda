# LaTex Lambda

##### Nix
You could optionally prepare the build environment using the [Nix package manager](https://nixos.org/nix).

1) Install Nix

```bash
curl https://nixos.org/nix/install | sh
```

2) Enter nix shell to download and install the required software as specified in `./shell.nix`

```bash
nix-shell
```

## Usage

```text
Input:
   zip file that contains
      a. "document.tex" file: the tex file is to be used to generate pdf file
      b.  all those files that are referred from document.tex file.
output:
   path for the pdf file to be written.
```
#### Invoke via commandline
Requirement: aws cli
Command:
```bash
aws lambda invoke --region=ap-southeast-2 --function-name latex-lambda-prod --payload '{"input_url":"XXXX.zip", "output_url":"YYYY.pdf"}' output.json
```
**_Preferred method is "signed"_** 
```
if input is signed
   "XXXX.zip" = "https://latex-lambda-test-data-prod.s3.amazonaws.com/test-data.zip?AWSAccessKeyId=AKIAIJFNJEMML3AZFO7A&Signature=dDHuTjBKKOH8sBfQAxOEQ2JkwBM%3D&Expires=1554094466"
and
   "yyyy.pdf" = "https://latex-lambda-test-data-prod.s3.amazonaws.com/test-data.pdf?AWSAccessKeyId=AKIAIJFNJEMML3AZFO7A&Signature=WCaKfhJZjxlq7H855kSOmrKMdVY%3D&Expires=1554094474"
```

```
if input is not signed
Should have appropriate permission for reading and writing.
   "XXXX.zip" = "http://s3-ap-southeast-2.amazonaws.com/testmazfile/ALIC.zip"
and
   "yyyy.pdf" = "http://s3-ap-southeast-2.amazonaws.com/testmazfile/ALIC.pdf"
```

#### Example
Example of tex and the supporting files can be found in
https://bitbucket.org/geoscienceaustralia/latex-lambda/src/master/test/unit_test/data/


