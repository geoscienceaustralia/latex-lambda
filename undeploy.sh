#!/usr/bin/env bash

set -euo pipefail

function usage {
    echo "Usage: ./undeploy.sh [-d|--dry-run] <env>
        where
        -d, --dry-run
            do not run \"terraform apply\"
        env
            deployment environment, eg., dev, test, or prod"
    exit 1
}

while [[ $# -gt 0 ]]; do
    case $1 in
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        dev|dev1|test|prod )
            env=$1
            shift
            ;;
        * )
            echo "Unknown option: $1"
            usage
            ;;
    esac
done

if [ -z ${env:-} ]; then
    echo "Error: deployment environment is empty."
    usage
fi

export TF_VAR_environment=$env
export TF_VAR_region=$AWS_DEFAULT_REGION


if [[ $env == "prod" || $env == "test" ]];
then
    env_suffix=-prod
else
    env_suffix=
fi

export TF_VAR_tf_state_bucket="geodesy-operations-terraform-state"$env_suffix
export TF_VAR_tf_state_table="geodesy-operations-terraform-state"$env_suffix

cd `dirname "$(realpath "$0")"`
cd aws/terraform

terraform init \
    -backend-config "bucket=$TF_VAR_tf_state_bucket" \
    -backend-config "dynamodb_table=$TF_VAR_tf_state_table" \
    -backend-config "region=$TF_VAR_region" \
    -backend-config "key=latex-lambda/$TF_VAR_environment/terraform.tfstate"

terraform get

if [ ${dryRun:-false} ]; then
    terraform destroy -auto-approve -var-file=$TF_VAR_environment.tfvars
else
    terraform plan -destroy -var-file=$TF_VAR_environment.tfvars
fi
