#!/usr/bin/env bash

set -e

function usage {
    echo "Usage: ./deploy.sh [-d|--dry-run] <env>
        where
        -d, --dry-run
            do not run \"terraform apply\"
        env
            deployment environment, eg., dev, test, or prod"
    exit 1
}

while [[ $# -gt 0 ]]; do
    case $1 in
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        dev|test|prod )
            env=$1
            shift
            ;;
        * )
            echo "Unknown option: $1"
            usage
            ;;
    esac
done

if [ -z $env ]; then
    echo "Error: deployment environment is empty."
    usage
fi

export TF_VAR_environment=$env
export TF_VAR_region=$AWS_REGION

if [[ $env == "prod" || $env == "test" ]];
then
    env_suffix="-prod"
fi

export TF_VAR_tf_state_bucket="geodesy-operations-terraform-state"$env_suffix
export TF_VAR_tf_state_table="geodesy-operations-terraform-state"$env_suffix

cd `dirname "$(realpath "$0")"`
cd aws/terraform

rm -f main_override.tf
find . -name terraform.tfstate -exec rm {} \;

terraform init \
    -backend-config "bucket=$TF_VAR_tf_state_bucket" \
    -backend-config "dynamodb_table=$TF_VAR_tf_state_table" \
    -backend-config "region=$TF_VAR_region" \
    -backend-config "key=latex-lambda/$TF_VAR_environment/terraform.tfstate"

terraform get

if [ -z "$dryRun" ]; then
    terraform apply -auto-approve 
else
    terraform plan 
fi
