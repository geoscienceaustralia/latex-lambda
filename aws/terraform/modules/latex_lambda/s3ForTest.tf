locals {
  bucket_name = "${var.application}-test-data-${var.environment}"
}

resource "aws_s3_bucket" "latex_lambda_test_data" {
  bucket = local.bucket_name
}

resource "aws_s3_bucket_policy" "latex_lamabda_test_data" {
  bucket = aws_s3_bucket.latex_lambda_test_data.id

  policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
           "Principal": {
              "AWS": [
                    "arn:aws:iam::${var.account_id}:root"
                    ]
           },
          "Action": [
            "s3:Put*",
            "s3:Get*",
            "s3:DeleteObjectVersion",
            "s3:ListBucket"
          ],
          "Resource": [
            "arn:aws:s3:::${local.bucket_name}/*",
            "arn:aws:s3:::${local.bucket_name}"
          ]
        }
      ]
    }
  EOF
}
