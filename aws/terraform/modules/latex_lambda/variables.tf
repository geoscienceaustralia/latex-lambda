variable "region" {}
variable "application" {}
variable "environment" {}
variable "repo_root_path" {}
variable "account_id" {}

variable "accounts" {
  description = "Allow accounts to access latex-lambda"
  type        = list(string)
  default     = [
      "688660191997", # geodesy-operations
      "623223935732", # geodesy-operations-prod
  ]
}
