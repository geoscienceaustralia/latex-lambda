locals {
  lambda_name         = "${var.application}-${var.environment}"
  lambda_package_name = "latex-lambda.zip"
  lambda_package_path = "${var.repo_root_path}/${local.lambda_package_name}"
}

resource "aws_iam_role" "latex_lambda" {
  name = "${var.application}-${var.environment}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

# Allow us to export logs to cloudwatch
resource "aws_iam_role_policy" "cloudwatch_logs" {
  role = aws_iam_role.latex_lambda.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    }
  ]
}
EOF
}

resource "aws_lambda_permission" "permission_account" {
  count         = length(var.accounts)
  statement_id  = "AllowExecutionFrom_${element(var.accounts, count.index)}"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.latex_lambda.function_name
  principal     = "arn:aws:iam::${element(var.accounts, count.index)}:root"
}

resource "aws_s3_bucket" "latex_lambda" {
  bucket = "${var.application}-source-${var.environment}"
}

resource "aws_s3_object" "latex_lambda" {
  bucket = aws_s3_bucket.latex_lambda.id
  key    = local.lambda_package_name
  source = local.lambda_package_path
  etag   = filebase64sha256(local.lambda_package_path)
}

resource "aws_lambda_function" "latex_lambda" {
  s3_bucket         = aws_s3_object.latex_lambda.bucket
  s3_key            = aws_s3_object.latex_lambda.key
  s3_object_version = aws_s3_object.latex_lambda.version_id
  memory_size       = 2048
  timeout           = 60
  function_name     = "${var.application}-${var.environment}"
  role              = aws_iam_role.latex_lambda.arn
  handler           = "lambda_function.lambda_handler"
  source_code_hash  = filebase64sha256(local.lambda_package_path)
  runtime           = "python3.7"
  depends_on        = [aws_cloudwatch_log_group.lambda_log_group]
}

resource "aws_cloudwatch_log_group" "lambda_log_group" {
  name = "/aws/lambda/${local.lambda_name}"
}
