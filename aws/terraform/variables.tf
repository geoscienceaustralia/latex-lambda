variable "region" {}

variable "tf_state_bucket" {
  description = "Name of terraform state S3 bucket."
}

variable "tf_state_table" {
  description = "Name of terraform state lock DDB table."
}

variable "environment" {
  description = "Deployment environment. Suffixes most created objects."
}

variable "application" {
  description = "Application name. Used as webapp S3 bucket name, suffixed by the deployment environment name."
  default     = "latex-lambda"
}
