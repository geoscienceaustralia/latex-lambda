data "aws_caller_identity" "current" {}

terraform {
  backend "s3" {}
}

locals {
  repo_root_path = "${path.root}/../.."
}

module "latex-lambda" {
  source = "./modules/latex_lambda/"

  repo_root_path = local.repo_root_path
  environment    = var.environment
  application    = var.application
  region         = var.region
  account_id     = data.aws_caller_identity.current.account_id
}
