import os
import subprocess
import sys
import pytest

TEMP_DIRECTORY = "data"


@pytest.fixture(scope="session", autouse=True)
def start_at_beginning():

    if not os.path.exists(TEMP_DIRECTORY):
        os.makedirs(TEMP_DIRECTORY)

    # run docker to create pdf file

    print("*******************")
    print(sys.version_info[0])
    print("*******************")

    docker_run_cmd = (
        "docker run --rm -v $(pwd)/" +
        TEMP_DIRECTORY +
        ":/tmp/latex geoscienceaustralia/latexlambda latexmk -f -verbose  -interaction=batchmode -pdf -output-directory=/tmp/latex /tmp/latex/document.tex")

    _, output = subprocess.getstatusoutput(docker_run_cmd)
    print(output)
