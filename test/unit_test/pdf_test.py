import os
import subprocess
from conftest import TEMP_DIRECTORY


def test_pdf_exist():
    assert os.path.exists(TEMP_DIRECTORY + "/document.pdf") == 1


def test_pdf_pages():
    subprocess.run(
        "pdftk "
        + TEMP_DIRECTORY
        + "/document.pdf burst output "
        + TEMP_DIRECTORY
        + "/document---page%03d.pdf",
        shell=True,
    )
    subprocess.run(
        "pdftk data/pdf/document.pdf burst output TestDocument---page%03d.pdf",
        shell=True,
    )

    for i in range(1, 6):
        cmd = (
            "compare -metric rmse "
            + TEMP_DIRECTORY
            + "/document---page"
            + str(i).zfill(3)
            + ".pdf "
            + "TestDocument---page"
            + str(i).zfill(3)
            + ".pdf  null:"
        )
        output = subprocess.getstatusoutput(cmd)
        status = str(output[0])
        if status == "1":
            string_output = str(output[1])
            diff_percentage = float(string_output[string_output.find("(") + 1: string_output.find(")")]) * 100
            print("PDF page is different by " + str(diff_percentage) + "%")
            # pdf is good if the chage is less then 2%
            status = "0" if (diff_percentage < 2) else "1"
        assert status == "0"


def test_compare_different_page():
    i = 1  # first page
    j = 3  # 3rd page
    cmd = (
        "compare -metric rmse "
        + TEMP_DIRECTORY
        + "/document---page"
        + str(i).zfill(3)
        + ".pdf "
        + "TestDocument---page"
        + str(j).zfill(3)
        + ".pdf  null:"
    )
    output = subprocess.getstatusoutput(cmd)
    status = str(output[0])
    # compare  command to retun missmatch
    assert status == "1"
