import os
import subprocess
import inspect
import boto3
import pytest

S3_BUCKET_NAME = "latex-lambda-test-data-" + os.environ['DEPLOY_ENVIRONMENT']
TEST_DATA_ZIP_FILE = "test-data.zip"
TEST_DATA_PDF_FILE = "test-data.pdf"
TEST_DATA_DIRECTORY = "data"
S3_CLIENT = boto3.client('s3')


@pytest.fixture(scope="session", autouse=True)
def cleanup(request):
    def remove_test_data():
        S3_CLIENT.delete_object(Bucket=S3_BUCKET_NAME, Key=TEST_DATA_PDF_FILE)
        S3_CLIENT.delete_object(Bucket=S3_BUCKET_NAME, Key=TEST_DATA_ZIP_FILE)
    request.addfinalizer(remove_test_data)


def setup_files(output_file_name):
    # remove outputfile if exists
    if os.path.isfile(output_file_name):
        os.remove(output_file_name)

    # remove test data and pdf file from the bucket if exist
    S3_CLIENT.delete_object(Bucket=S3_BUCKET_NAME, Key=TEST_DATA_PDF_FILE)
    S3_CLIENT.delete_object(Bucket=S3_BUCKET_NAME, Key=TEST_DATA_ZIP_FILE)
    # copy test data to s3
    S3_CLIENT.upload_file(
        TEST_DATA_DIRECTORY +
        "/" +
        TEST_DATA_ZIP_FILE,
        S3_BUCKET_NAME,
        TEST_DATA_ZIP_FILE)


def get_signed_url(method, bucket_name, key_name, expires_in_second):
    url = ""
    if method == "POST":
        url = S3_CLIENT.generate_presigned_url(
            'put_object',
            Params={
                'Bucket': bucket_name,
                'Key': key_name,
                'ContentType': 'application/pdf'
            },
            ExpiresIn=expires_in_second,
            HttpMethod='PUT'
        )
    elif method == "GET":
        url = S3_CLIENT.generate_presigned_url(
            'get_object',
            Params={
                'Bucket': bucket_name,
                'Key': key_name,
            },
            ExpiresIn=expires_in_second
        )
    else:
        # not supported error. How to??
        pytest.fail("Not Supported method. " + method)
    return url

def check_pdf_pages(download_pdf_file):
    subprocess.run(
        "pdftk " +
        download_pdf_file +
        " burst output " +
        download_pdf_file +
        "---page%03d.pdf",
        shell=True)
    subprocess.run(
        "pdftk " +
        TEST_DATA_DIRECTORY +
        "/pdf/document.pdf burst output " +
        TEST_DATA_DIRECTORY +
        "/pdf/true---page%03d.pdf",
        shell=True)

    for i in range(1, 6):
        cmd = "compare -metric rmse " \
            + download_pdf_file \
            + "---page" \
            + str(i).zfill(3) + ".pdf " \
            + TEST_DATA_DIRECTORY  \
            + "/pdf/true---page" + str(i).zfill(3) + ".pdf  null:"
        output = subprocess.getstatusoutput(cmd)
        status = output[0]
        if status == 1:
            string_otput = str(output[1])
            diff_percentage = float(
                string_otput[string_otput.find("(") + 1:string_otput.find(")")]) * 100
            print("PDF page is different by " + str(diff_percentage) + "%")
            status = 0 if (diff_percentage < 2) else 1
    return status


def compare_different_page(download_pdf_file):
    i = 1  # first page
    j = 3  # 3rd page
    cmd = "compare -metric rmse " + download_pdf_file + "---page" + \
        str(i).zfill(3) + ".pdf " + TEST_DATA_DIRECTORY + "/pdf/true---page" + str(j).zfill(3) + ".pdf  null:"
    output = subprocess.getstatusoutput(cmd)
    status = output[0]
    # compare  command to retun missmatch
    return status


def invoke_lambda(output_file_name, input_url, output_url):
    cmd = "aws lambda invoke --region="    \
          + os.environ['AWS_REGION'] \
          + " --function-name latex-lambda-" + os.environ['DEPLOY_ENVIRONMENT']  \
          + " --cli-binary-format raw-in-base64-out" \
          + " --payload '{\"input_url\":\"" + input_url + "\", \"output_url\":\"" + output_url + "\"}' "   \
          + output_file_name
    print(cmd) 
    output = subprocess.getstatusoutput(cmd)
    status = output[0]
    return status


def test_invoke_lambda_with_valid_input():
    output_file_name = "output" + inspect.stack()[0][3]
    print("******************* " + output_file_name)
    setup_files(output_file_name)
    input_url = get_signed_url("GET", S3_BUCKET_NAME, TEST_DATA_ZIP_FILE, 3600)
    output_url = get_signed_url(
        "POST",
        S3_BUCKET_NAME,
        TEST_DATA_PDF_FILE,
        3600)
    status = invoke_lambda(output_file_name, input_url, output_url)

    # test pdf File
    download_pdf_file = output_file_name + '.pdf'
    S3_CLIENT.download_file(
        S3_BUCKET_NAME,
        TEST_DATA_PDF_FILE,
        download_pdf_file)
    assert status == 0 and check_pdf_pages(download_pdf_file) == 0 and compare_different_page(
        download_pdf_file) == 1 and '{"response": 200, "reason": "OK"}' in open(output_file_name).read()


def test_invoke_lambda_with_invalid_input():
    output_file_name = "output" + inspect.stack()[0][3]
    setup_files(output_file_name)
    input_url = get_signed_url("GET", S3_BUCKET_NAME, TEST_DATA_ZIP_FILE, 3600)
    # parameter is cganged so to get wrong url
    wrong_output_url = get_signed_url("GET", S3_BUCKET_NAME, TEST_DATA_PDF_FILE, 3600)

    status = invoke_lambda(output_file_name, input_url, wrong_output_url)
    assert status == 0 and '{"response": 403, "reason": "Forbidden"}' in open(
        output_file_name).read()


def test_invoke_lambda_with_expired_input_or_not_zipfile():
    output_file_name = "output" + inspect.stack()[0][3]
    setup_files(output_file_name)
    input_url = get_signed_url("GET", S3_BUCKET_NAME, TEST_DATA_ZIP_FILE, -1)
    # parameter is cganged so to get wrong url
    output_url = get_signed_url(
        "POST",
        S3_BUCKET_NAME,
        TEST_DATA_PDF_FILE,
        3600)
    status = invoke_lambda(output_file_name, input_url, output_url)
    assert status == 0 and '"errorMessage": "File is not a zip file"' in open(
        output_file_name).read()
