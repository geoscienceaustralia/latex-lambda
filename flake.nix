{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    gnss-common.url = "git+ssh://git@bitbucket.org/geoscienceaustralia/gnss-common";
    gnss-common.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
    self,
    nixpkgs,
    gnss-common,
  }: let
    project = "latex-lambda";
    forAllSystems = f:
      nixpkgs.lib.genAttrs
      gnss-common.lib.systems
      (
        system:
          f {
            pkgs = nixpkgs.legacyPackages.${system}.extend gnss-common.overlays.default;
            inherit system;
          }
      );
  in {
    formatter = forAllSystems ({system, ...}: gnss-common.formatter.${system});

    devShells = forAllSystems ({pkgs, ...}: rec {
      # Environment for custom pipeline `build-pipelines-docker-image`
      ciImage = pkgs.mkShellNoCC {
        name = "${project}-ci-image";

        packages = [
          # gnss-common overlay
          pkgs.docker-login
        ];

        shellHook = ''
          export PROJECT="${project}"
        '';
      };

      # Environment for all pipelines (except `build-pipelines-docker-image`)
      ci = pkgs.mkShellNoCC {
        name = "${project}-ci";

        inputsFrom = [ciImage];

        packages = [
          # nixpkgs
          pkgs.awscli2
          pkgs.bash
          pkgs.docker
          pkgs.ghostscript
          pkgs.imagemagick7
          pkgs.pdftk
          pkgs.python3
          pkgs.terraform_1
        ];

        shellHook = ''
          python3 -m venv python-env
          . ./python-env/bin/activate
          pip install -r ./requirements.txt
         '';
      };

      # Environment for developer workstations
      developer = pkgs.mkShellNoCC {
        name = project;

        inputsFrom = [ci];

        packages = [
          pkgs.python3Packages.pip
          pkgs.python3Packages.pylint
          pkgs.shellcheck
        ];
      };

      default = developer;
    });
  };
  nixConfig.bash-prompt-prefix = "(nix-shell:$name) ";
}
