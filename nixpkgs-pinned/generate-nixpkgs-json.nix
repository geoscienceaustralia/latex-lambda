{ revision ? null 
}:

let
  pkgs = import <nixpkgs> {};
  inherit (pkgs) stdenv;
  inherit (pkgs.stdenv.lib.asserts) assertMsg;

  owner = "NixOS";
  repo = "nixpkgs-channels";

  rev = if revision != null
    then revision
    else stdenv.lib.trivial.revisionWithDefault null;

  url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";

  archive = builtins.fetchTarball { inherit url; };
  archiveHash = "nix-hash --type sha256 --base32 ${archive}";

  json = builtins.toFile "json" ''
    { "url": "${url}"
    , "rev": "${rev}"
    , "owner": "${owner}"
    , "repo": "${repo}"
    }
  '';

in

assert (assertMsg (rev != null)
  "Not running NixOS? Please supply a revision of https://github.com/${owner}/${repo}");

pkgs.stdenv.mkDerivation rec {
  name = "generate-nixpkgs-json";

  buildInputs = [
    pkgs.jq
    pkgs.nix
  ];

  shellHook = ''
    set -eu
    sha256=$(nix-hash --type sha256 --base32 ${archive})
    jq .+="{\"sha256\":\"$sha256\"}" ${json} > ./nixpkgs-pinned.json
  '';
}
