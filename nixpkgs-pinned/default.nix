{ config ? {}
, overlays ? []
}:

let
  nixpkgsJson = builtins.fromJSON (builtins.readFile ../nixpkgs-pinned.json);

  nixpkgs = with nixpkgsJson; builtins.fetchTarball {
    url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
    inherit sha256;
  };

  pkgs = import nixpkgs { inherit config overlays; };
in
  pkgs
