#!/usr/bin/env bash

# Generate nixpkgs.json, identifying a specific commit of http://github.com/NixOS/nixpkgs-channels.

set -e

scriptDir="$(dirname "${BASH_SOURCE[0]}")"

rev="$1"

if [ -n "$rev" ]; then
    args="--argstr revision $rev"
else
    args=
fi

nix-shell $args --pure --command true "$scriptDir"/generate-nixpkgs-json.nix
