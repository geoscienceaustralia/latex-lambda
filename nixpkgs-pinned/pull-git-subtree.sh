#!/usr/bin/env bash

# Update nixpkgs-pinned.git subtree. 

set -e

scriptDir="$(dirname "${BASH_SOURCE[0]}")"
cd "$scriptDir"/..
git subtree pull --prefix ../nixpkgs-pinned "$(git remote get-url origin)" master --squash
