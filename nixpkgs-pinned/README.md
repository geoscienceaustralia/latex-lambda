# nixpkgs-pinnned

## About

If you are using [nix-shell](https://nixos.org/nix/manual/) to spin up your
build environments, then you could use the scripts in this repository to
manage your projects' revision of [nixpkgs](https://nixos.org/nixpkgs/).

In `shell.nix`, replace

```
let
  bootstrap = import <nixpkgs> {};
  pkgs = import (bootstrap.fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs-channels";
    rev = "56fb68dcef4";
    sha256 = "0im8l87nghsp4z7swidgg2qpx9mxidnc0zs7a86qvw8jh7b6sbv2";
  }) {};
in
...
```

with

```nix
let
  pkgs = import ./nixpkgs-pinned {};
in
...
```
## Setup

1) Add nixpkgs-pinned.git to your project.

```bash
  git subtree add --prefix nixpkgs-pinned git@bitbucket.org:geoscienceaustralia/nixpkgs-pinned.git master --squash
```

2) Generate `nixpkgs-pinned.json` in the root of your project.

```bash
./nixpkgs-pinned/generate-nixpkgs-json.sh <SHA1> # where SHA1 is a revision of http://gitub.com/NixOS/nixpkgs-channels.
```

You can leave out the rivision if you are on NixOS and wish to use your system's current
revision, which is given by `nixos-version --revision`.

3) Pin the revision of nixpkgs in your project's `shell.nix` file.
```nix
let
  pkgs = import ./nixpkgs-pinned {};
in
...
```

## Update nixpkgs

1) Update `nixpkgs-pinned.json`
```bash
./nixpkgs-pinned/generate-nixpkgs-json.sh <SHA1>
```

2) Re-enter nix-shell.

## Pull in latest nixpkgs-pinned.git

```bash
./nixpkgs-pinned/pull-git-subtree.sh
```

